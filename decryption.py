from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
import binascii

def read_input(filename):
  input_file = open(filename, "r")

  input_list = []
  for line in input_file:
    stripped_line = line.strip()
    input_list.append(stripped_line)

  input_file.close()
  return input_list

def decrypt(ciphertext, key, iv):
  data_bytes = binascii.unhexlify(bytes(ciphertext, "utf-8"))
  AES_object = AES.new(key, AES.MODE_CBC, iv)
  raw_bytes = AES_object.decrypt(data_bytes)
  extracted_bytes = unpad(raw_bytes, AES.block_size)
  return extracted_bytes

def main():
  input_list = read_input("encrypted.txt")
  output_list = []
  
  key = pad(b"specialkey", AES.block_size)
  iv = pad(b"specialiv", AES.block_size)

  for line in input_list:
    output_list.append(decrypt(line, key, iv))

  output_file = open("decrypted.txt", "w")
  for i in range(len(output_list)):
    output_file.write(output_list[i].decode("ascii")+"\n")
  output_file.close()

if __name__ == "__main__":
  main()