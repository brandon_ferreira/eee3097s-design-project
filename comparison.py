def read_input(filename):
    input_file = open(filename, "r")

    input_list = []
    for line in input_file:
      stripped_line = line.strip()
      input_list.append(stripped_line)

    input_file.close()
    return input_list

def main():
    input_list = read_input(input("Enter filename 1:\n"))
    output_list = read_input(input("Enter filename 2:\n"))

    correct = 0

    for i in range(len(input_list)):
        if (input_list[i] == output_list[i]):
            correct = correct+1
    print(str(100*correct/len(input_list)) + "% of the data was restored")

if __name__ == "__main__":
  main()