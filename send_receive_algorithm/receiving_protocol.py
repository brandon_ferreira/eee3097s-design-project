from Crypto.Util.Padding import pad, unpad
from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
import binascii
import zlib
import sys

def decompress(plaintext):
  decomp = zlib.decompress(plaintext)
  return decomp

def read_input(filename):
  input_file = open(filename, "r")

  input_list = []
  for line in input_file:
    stripped_line = line.strip()
    input_list.append(stripped_line)

  input_file.close()
  return input_list

def decrypt(ciphertext, key, iv):
  data_bytes = binascii.unhexlify(ciphertext)
  AES_object = AES.new(key, AES.MODE_CBC, iv)
  raw_bytes = AES_object.decrypt(data_bytes)
  extracted_bytes = unpad(raw_bytes, AES.block_size)
  return extracted_bytes

def main():
  input_list = read_input("encrypted_compressed.txt")
  output_list = []
  decrypt_list = []

  key = pad(b"specialkey", AES.block_size)
  iv = pad(b"specialiv", AES.block_size)

  for line in input_list:
    decrypt_list.append(decrypt(line, key, iv))
  
  for line in decrypt_list:
    output_list.append(decompress(line))

  output_file = open("decrypted_decompressed.csv", "w")
  for i in range(len(output_list)):
    output_file.write(output_list[i].decode("utf-8")+"\n")
  output_file.close()

if __name__ == "__main__":
  main()
