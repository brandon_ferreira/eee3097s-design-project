from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
import binascii

def read_input(filename):
  input_file = open(filename, "r")

  input_list = []
  for line in input_file:
    stripped_line = line.strip()
    input_list.append(stripped_line)

  input_file.close()
  return input_list

def encrypt(plaintext, key, iv):
  data_bytes = bytes(plaintext, "utf-8")
  padded_bytes = pad(data_bytes, AES.block_size)

  AES_object = AES.new(key, AES.MODE_CBC, iv)
  ciphertext = AES_object.encrypt(padded_bytes)
  return ciphertext

def main():
  input_list = read_input("sample.txt")
  output_list = []
  
  key = pad(b"specialkey", AES.block_size)
  iv = pad(b"specialiv", AES.block_size)

  for line in input_list:
    output_list.append(encrypt(line, key, iv))

  output_file = open("encrypted.txt", "w")
  for i in range(len(output_list)):
    output_file.write(binascii.hexlify(output_list[i]).decode("ascii")+"\n")
  output_file.close()

if __name__ == "__main__":
  main()