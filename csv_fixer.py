
def read_input(filename):
    input_file = open(filename, "r")

    input_list = []
    for line in input_file:
      stripped_line = line.strip()
      input_list.append(stripped_line)

    input_file.close()
    return input_list

def main():
    input_list = read_input(input("Enter filename:\n"))
    output_list = []

    for i in range(len(input_list)):
        output_list.append("")

    output_list[0] = input_list[0]
    output_list[1] = input_list[1]
    output_list[len(input_list)-1] = input_list[len(input_list)-1]

    for i in range(2, len(input_list)-1):
        output_list[i] = input_list[i].replace(" ", ", ")
    
    output_file = open("test_correct.csv", "w")
    for i in range(len(output_list)):
        output_file.write(output_list[i]+"\n")
    output_file.close()   


if __name__ == "__main__":
    main()