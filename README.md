# EEE3097S - Design Project

This Project Deals with the design project for EEE3097S offered by UCT. Group memebers include Sina Davachi Omoumi (dvcsin001) and Brandon Ferreira (frrbra002)

## Getting started

The repository is split into various sub-folders named according to their section. Within each sub-folder are other sub-folders containing sort information related to that section.

## Contributing

Only group memebers are permitted to add content to this repository. The Repo information is backed up in the event of lose of information.


